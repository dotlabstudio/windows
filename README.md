# Windows - Do NOT use on real hardware - only on virtual machines project

This is a public repo to all things relating to the worst operating system on earth called Windows.

But it is ok to use on virtual machines.

If you are forced to use Windows, use portable apps on a usb thumb drive keyring:

see the homepage at https://portableapps.com/

For a good software packagement system use chocolatey:

see the homepage at https://chocolatey.org/

to install chocolatey
With PowerShell, you must ensure Get-ExecutionPolicy is not Restricted. We suggest using Bypass to bypass the policy to get things installed or AllSigned for quite a bit more security.

```
Run Get-ExecutionPolicy
```

If it returns Restricted, then run 

```
Set-ExecutionPolicy AllSigned
```

or 

```
Set-ExecutionPolicy Bypass -Scope Process.
```

then run this command:

```
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
```

or for the lazy people, use winget - it should be preinstalled in Windows 10/11

see the https://winget.run/ homepage

This repo mainly contains an automated script to autoinstall Windows 10/11 pro for my virtual machines.

To create your own windows answer file go to:

https://www.windowsafg.com/index.html
